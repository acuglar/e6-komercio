def pagination(products, per_page, page):
    per_page = int(per_page) if per_page else 5
    page = int(page) if page else 1
    
    first = (page - 1) * per_page
    last = per_page * page
    
    next_page = page + 1 
    previous_page = page - 1 if page > 1 else 1   
    
    return {
        'products': products[first:last],
        'amount': per_page,
        'page': page,
        'next_page': 'localhost:5000/products?page='
        + str(next_page)
        + '&per_page='
        + str(per_page),
        'previous_page': 'localhost:5000/products?page='
        + str(previous_page)
        + '&per_page='
        + str(per_page)
    }