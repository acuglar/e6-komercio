from flask import Flask, request, jsonify
from komercio import lista_de_produtos
from services import pagination


app = Flask(__name__)


@app.route('/products')
def list_products():
    page = request.args.get('page')
    per_page = request.args.get('per_page')

    response = pagination(lista_de_produtos, page, per_page)

    return jsonify(response)


@app.route('/products/<product_id>')
def get_product(product_id):
    for product in lista_de_produtos:
        if product['id'] == int(product_id):
            return product
    return {'erro': 'id não encontrado'}, 404